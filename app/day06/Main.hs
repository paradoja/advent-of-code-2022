module Main (main) where

import CommunicationSystem
import Data.Maybe (fromJust)

main :: IO ()
main = do
  input <- getContents
  let packetMarker = fromJust . findStartOfPacketMarker $ input
      messageMarker = fromJust . findStartOfMessageMarker $ input
  putStrLn "Part one"
  print packetMarker
  putStrLn "Part two"
  print messageMarker

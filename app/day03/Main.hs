module Main (main) where

import Data.List.Split (chunksOf)
import RucksackReview (getCommon, getCommonBadge, getPriority)

main :: IO ()
main = do
  input <- getContents
  let ls = lines input
  putStrLn "Part one"
  print $ sum $ map (getPriority . getCommon) ls
  putStrLn "Part two"
  print $ sum $ map (getPriority . getCommonBadge) $ chunksOf 3 ls

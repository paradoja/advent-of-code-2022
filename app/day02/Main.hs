module Main (main) where

import RPSGame (inversePlay)
import RPSScore

main :: IO ()
main = do
  input <- getContents
  let ls = lines input
  putStrLn "Part one"
  print $
    totalScore $
      map (parseLinePart1 . words) ls
  putStrLn "Part two"
  print $
    totalScore $
      map (parseLinePart2 . words) ls
  where
    parseLinePart1 (f : s : _) = (read f, read s)
    parseLinePart1 _ = error "Wrong line"
    parseLinePart2 (f : s : _) =
      let firstRPS = read f
       in (firstRPS, inversePlay firstRPS $ read s)
    parseLinePart2 _ = error "Wrong line"

module Main (main) where

import SectionAssignment (hasStrictSubset, hasSubset, lineToAssignmentPairs)

main :: IO ()
main = do
  input <- getContents
  let ls = lines input
  putStrLn "Part one"
  print $ length $ filter (uncurry hasStrictSubset . lineToAssignmentPairs) ls
  putStrLn "Part two"
  print $ length $ filter (uncurry hasSubset . lineToAssignmentPairs) ls

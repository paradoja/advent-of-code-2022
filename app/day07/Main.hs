module Main (main) where

-- We're fancy around here so we add a command line parser and interpreter,
-- which produces an also fancy filesystem with the property to check it's own
-- size.

import CommandLine.Interpreter
import CommandLine.Parser
import Data.Attoparsec.ByteString.Char8
import qualified Data.ByteString.Char8 as BS
import Data.Foldable
import Data.Maybe (catMaybes, fromJust, fromMaybe)
import FileSystem

main :: IO ()
main = do
  input <- getContents
  let ls = BS.pack input
      listing = fromJust . maybeResult . flip feed mempty . parse parseListing $ ls
      elfFileSystem = runElfListing listing
      sizes = treeToSizeTree elfFileSystem
      sum100k =
        foldl
          ( \acc size' ->
              let size = fromMaybe 0 size'
               in if size <= 100000
                    then acc + size
                    else acc
          )
          0
          sizes
      currentSizeUsed = fromJust $ treeInfo sizes
      availableSpace = 70000000 - currentSizeUsed
      howMuchSpaceWeNeed = 30000000 - availableSpace
      minimumAcceptable = minimum $ filter (>= howMuchSpaceWeNeed) $ catMaybes $ toList sizes
  putStrLn "Part one"
  print sum100k
  putStrLn "Part two"
  print . catMaybes $ toList sizes
  print currentSizeUsed
  print howMuchSpaceWeNeed
  print minimumAcceptable

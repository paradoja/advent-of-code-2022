module Main (main) where

import TreeVisibility

main :: IO ()
main = do
  input <- getContents
  let ls = lines input
  putStrLn "Part one"
  print $ treeVisibility ls
  putStrLn "Part two"
  print $ maximizingTrees ls

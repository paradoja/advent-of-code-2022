{-# OPTIONS_GHC -fno-warn-incomplete-uni-patterns #-}

module Main (main) where

import CargoCrane

main :: IO ()
main = do
  input <- getContents
  -- all in BS to be faster?
  let ls = lines input
      (elfMap', instructions) = break (== "") ls
      originalElfMap = buildCargo $ init elfMap'
      instructionList = tail instructions
      instructions9000 = readCraneInstructions instructionSet9000 instructionList
      instructions9001 = readCraneInstructions instructionSet9001 instructionList
      finalMap1 = foldl followInstruction originalElfMap instructions9000
      finalMap2 = foldl followInstruction originalElfMap instructions9001
  putStrLn "Part one"
  putStrLn $ fmap head finalMap1
  putStrLn "Part two"
  putStrLn $ fmap head finalMap2

module Main (main) where

import Data.List (nub)
import RopeBridge

main :: IO ()
main = do
  input <- getContents
  let ls = lines input
      dirs = fmap read ls
      tailPositions = snd $ runMotions dirs
  putStrLn "Part one"
  print . length . nub $ tailPositions
  putStrLn "Part two"
  -- print $ maximizingTrees ls
  print "tbd"

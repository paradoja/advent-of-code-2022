module Main (main) where

import CalorieCounter (countCalories)

main :: IO ()
main = do
  input <- getContents
  print $ sum $ countCalories 3 $ lines input

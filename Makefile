# AoC 2022
#
# @file
# @version 0.1
.PHONY: copy

copy: # default copy inputs to current dir
	mv ~/Downloads/input .

# end

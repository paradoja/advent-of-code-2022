module SectionAssignment (hasStrictSubset, hasSubset, lineToAssignmentPairs, AssignmentPair) where

import Data.List.Split (splitOn)

type AssignmentPair = (Int, Int)

strToPair :: String -> AssignmentPair
strToPair str = case map read $ splitOn "-" str of
  f : s : _ -> (f, s)
  _ -> error "Invalid range"

lineToAssignmentPairs :: String -> (AssignmentPair, AssignmentPair)
lineToAssignmentPairs str = case map strToPair $ splitOn "," str of
  f : s : _ -> (f, s)
  _ -> error "Invalid range"

strictSubset :: AssignmentPair -> AssignmentPair -> Bool
strictSubset (a, b) (x, y) = a >= x && b <= y

subset :: AssignmentPair -> AssignmentPair -> Bool
subset (a, b) (x, y) = a >= x && a <= y || b >= x && b <= y

hasStrictSubset :: AssignmentPair -> AssignmentPair -> Bool
hasStrictSubset a b = strictSubset a b || strictSubset b a

hasSubset :: AssignmentPair -> AssignmentPair -> Bool
hasSubset a b = subset a b || subset b a

module TreeVisibility (treeVisibility, maximizingTrees, scenicScoreFor) where

import Data.List (transpose)

{-# ANN treeVisibility "HLint: ignore Move guards forward" #-}
treeVisibility :: [[Char]] -> Int
treeVisibility elfMap =
  let rows = length elfMap - 1
      map2 = transpose elfMap
      columns = length map2 - 1
   in length $
        filter
          id
          [ treeVisible row c || treeVisible column r
            | r <- [0 .. rows],
              c <- [0 .. columns],
              let row = elfMap !! r,
              let column = map2 !! c
          ]

treeVisible :: [Char] -> Int -> Bool
treeVisible row i =
  let (l, r') = splitAt i row
      e = head r'
      r = tail r'
   in all (< e) l || all (< e) r

halfScenicScore :: [Char] -> Int -> Int
halfScenicScore row i =
  let (l, r') = splitAt i row
      e = head r'
      r = tail r'
      visibleL = takeWhile (< e) $ reverse l
      visibleR = takeWhile (< e) r
   in amount visibleL l * amount visibleR r
  where
    amount taken ori =
      length taken
        + if length taken == length ori then 0 else 1

scenicScoreFor :: [[Char]] -> [[Char]] -> Int -> Int -> Int
scenicScoreFor m mt r c =
  let row = m !! r
      column = mt !! c
   in halfScenicScore row c * halfScenicScore column r

{-# ANN maximizingTrees "HLint: ignore Move guards forward" #-}
maximizingTrees :: [[Char]] -> Int
maximizingTrees elfMap =
  let rows = length elfMap - 1
      map2 = transpose elfMap
      columns = length map2 - 1
   in maximum
        [ scenicScoreFor elfMap map2 r c
          | r <- [0 .. rows],
            c <- [0 .. columns]
        ]

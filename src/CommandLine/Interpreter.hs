module CommandLine.Interpreter (runElfListing) where

import CommandLine.Parser
  ( CdTarget (Root, TargetDir, Up),
    Command (CD, LS),
    ElfListing,
    ElfListingItem,
    ListingItem (Command, FS),
  )
import Control.Monad.State (MonadState (state), State, runState)
import FileSystem
  ( ElfFSZipper,
    ElfFileSystem,
    FSZipper (FSZipper),
    FileSystem (Directory),
    fszDown,
    fszUp,
    fszUpToRoot,
    newEFS,
  )

-- We of course build the interpreter on top of a state monad, we're not going
-- to do a simpler thing now after all the rest. We, though, don't add error
-- checking. Elfs make no mistakes - at least on part 1s.

data Mode = Commands | Files

data IState = IS ElfFSZipper Mode

runElfListing :: ElfListing -> ElfFileSystem
runElfListing listing =
  let (listZips, _) = runState (traverse run listing) $ IS (FSZipper newEFS []) Files
      (FSZipper fs _) = fszUpToRoot $ last listZips
   in fs

run :: ElfListingItem -> State IState ElfFSZipper
run (Command (CD target)) = state $ \is ->
  let (IS z m) = adjustModeForCommand is
      nextZipper = case target of
        Root -> fszUpToRoot z
        Up -> fszUp z
        TargetDir name -> fszDown z name
   in (nextZipper, IS nextZipper m)
run (Command LS) = state $ \is ->
  let (IS z _) = adjustModeForCommand is
   in (z, IS z Files)
run (FS currentItem) = state $ \(IS oldZ _) ->
  let newZ = addCurrentItem currentItem oldZ
   in (newZ, IS newZ Files)

-- We change the order of the files; no one cares, but I guess we do.
adjustModeForCommand :: IState -> IState
adjustModeForCommand (IS (FSZipper (Directory name ls) p) Files) =
  IS (FSZipper (Directory name (reverse ls)) p) Commands
adjustModeForCommand is = is

addCurrentItem :: FileSystem a -> FSZipper a -> FSZipper a
addCurrentItem item (FSZipper (Directory name ls) p) =
  FSZipper (Directory name $ item : ls) p
addCurrentItem _ _ = error "Trying to add files to a file"

{-# LANGUAGE OverloadedStrings #-}

---- We have effectively 3 languages here - the command language, the ls results
---- language and the shell listing language. We build parsers for each, all in
---- this file for ease.
module CommandLine.Parser
  ( CdTarget (..),
    Command (..),
    ElfListing,
    ElfListingItem,
    ListingItem (..),
    parseListing,
  )
where

import Control.Monad (join)
import Data.Attoparsec.ByteString.Char8
  ( Parser,
    char,
    choice,
    digit,
    isAlpha_ascii,
    isDigit,
    many',
    many1,
    skipMany,
    skipSpace,
    string,
    takeWhile1,
  )
import qualified Data.ByteString.Char8 as BS
import Data.Functor ((<&>))
import FileSystem (ElfFileSystem, FileSystem (..), Size)

type ElfListingItem = ListingItem Size

type ElfListing = [ElfListingItem]

data ListingItem a = Command Command | FS (FileSystem a)
  deriving (Show, Eq)

data Command = LS | CD CdTarget
  deriving (Show, Eq)

data CdTarget = Root | Up | TargetDir BS.ByteString
  deriving (Show, Eq)

---- ElfListing parsers
parseListing :: Parser ElfListing
parseListing =
  join
    <$> many'
      ( char '$'
          >> skipSpace
          >> choice [parseLsList, parseCommandCd]
            <* skipEoL
      )

parseCommandCd :: Parser ElfListing
parseCommandCd = (: []) . Command <$> parseCd <* skipEoL

parseLsList :: Parser ElfListing
parseLsList = do
  ls <- Command <$> parseLs
  skipEoL
  lsListing <- fmap FS <$> parseLsResult
  skipEoL
  return $ ls : lsListing

---- Command parsers
parseCd :: Parser Command
parseCd = do
  _ <- string "cd"
  skipSpace
  target <- choice [root, up, dir]
  return $ CD target
  where
    up = string ".." >> return Up
    root = string "/" >> return Root
    dir = validName <&> TargetDir

parseLs :: Parser Command
parseLs = string "ls" >> return LS

---- ElfFileSystem parsers
parseLsResult :: Parser [ElfFileSystem]
parseLsResult = many' $ choice [parseFile, parseDir] <* skipEoL

parseFile :: Parser ElfFileSystem
parseFile = do
  size <- read <$> many1 digit
  skipSpace
  name <- validName
  return $ File name size

parseDir :: Parser ElfFileSystem
parseDir = do
  _ <- string "dir"
  skipSpace
  validName <&> flip Directory []

---- utils
validName :: Parser BS.ByteString
validName = takeWhile1 $ or . sequenceA [(== '.'), isDigit, isAlpha_ascii]

skipEoL :: Parser ()
skipEoL = skipMany (char '\n')

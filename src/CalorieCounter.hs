module CalorieCounter (countCalories) where

import Data.List (sort)

countCalories :: Int -> [String] -> [Int]
countCalories maxes = snd . foldl iter (0, replicate maxes 0)
  where
    iter (acc, currentMaxes) "" = (0, take maxes $ reverse $ sort $ acc : currentMaxes)
    iter (acc, currentMaxes) str = (acc + read str, currentMaxes)

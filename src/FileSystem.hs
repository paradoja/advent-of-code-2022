{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE OverloadedStrings #-}

module FileSystem where

import qualified Data.ByteString.Char8 as BS

type Name = BS.ByteString

type Size = Int

type ElfFileSystem = FileSystem Size

data FileSystem fileData = Directory Name [FileSystem fileData] | File Name fileData
  deriving (Show, Eq, Foldable)

-- I don't want a catamorphism
data GenericTree a i = Leaf (FileSystem a) i | Branch (FileSystem a) i [GenericTree a i]
  deriving (Show, Eq, Foldable)

totalSize :: ElfFileSystem -> Size
totalSize (File _ i) = i
totalSize (Directory _ fs) = sum $ fmap totalSize fs

fsName :: FileSystem a -> BS.ByteString
fsName (File name _) = name
fsName (Directory name _) = name

-- Quick hack to play with the filesystem info
treeToSizeTree :: ElfFileSystem -> GenericTree Size (Maybe Int)
treeToSizeTree d@(Directory _ ls) = Branch d (Just $ totalSize d) (fmap treeToSizeTree ls)
treeToSizeTree f = Leaf f Nothing

treeInfo :: GenericTree a i -> i
treeInfo (Leaf _ i) = i
treeInfo (Branch _ i _) = i

-- ElfFileSystem all start from /
newEFS :: ElfFileSystem
newEFS = Directory "/" []

-- We build a Zipper because we can and to make it fancy (that's the word of the
-- day) to build a filesystem.

---- FSZipper
data FSOpenDir a = FSOpenDir Name [FileSystem a] [FileSystem a]

data FSZipper a = FSZipper {fsPointer :: FileSystem a, fsPathUp :: [FSOpenDir a]}

type ElfFSZipper = FSZipper Int

fszDown :: FSZipper a -> BS.ByteString -> FSZipper a
fszDown (FSZipper (Directory currentName siblings) pathUp) targetName =
  let (left, right') = break ((== targetName) . fsName) siblings
      target = head right' -- avoiding warnings the lazy way
      right = tail right'
   in FSZipper target $ FSOpenDir currentName left right : pathUp
fszDown _ _ = error "Trying to go down a file?"

fszUp :: FSZipper a -> FSZipper a
fszUp (FSZipper currentItem ((FSOpenDir name left right) : pathUp)) =
  FSZipper (Directory name $ left ++ (currentItem : right)) pathUp
fszUp _ = error "Going up when there's no path"

fszUpToRoot :: FSZipper a -> FSZipper a
fszUpToRoot current@(FSZipper _ []) = current
fszUpToRoot current@(FSZipper _ _) = fszUpToRoot $ fszUp current

module RPSScore (totalScore) where

import RPSGame (RPS (..), RPSResult (..), play)

scoreShape :: RPS -> Int
scoreShape Rock = 1
scoreShape Paper = 2
scoreShape Scissors = 3

scoreOutcome :: RPSResult -> Int
scoreOutcome Win = 6
scoreOutcome Draw = 3
scoreOutcome Lose = 0

totalScore :: [(RPS, RPS)] -> Int
totalScore = foldl roundScore 0
  where
    roundScore acc (them, me) = acc + scoreShape me + scoreOutcome (play me them)

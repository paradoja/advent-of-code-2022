module RopeBridge where

-- We create a State monad because yes (and who knows what part 2 may bring) and
-- it makes it easy to get the list of places at the end. We also build a simple
-- read parser for the Motions.

import Control.Monad.State (MonadState (state), State, runState)
import qualified Text.ParserCombinators.ReadP as RP
import Text.ParserCombinators.ReadPrec (get, lift)
import Text.Read
  ( Read (readListPrec, readPrec),
    readListPrecDefault,
  )

---- Movement

type Distance = Int

data Direction = U | D | R | L
  deriving (Show, Read, Eq)

data Motion = Motion Direction Distance
  deriving (Show, Eq)

instance Read Motion where
  readPrec = do
    dirChar <- get
    let dir = read [dirChar]
    _ <- get -- space
    distChar <- lift $ RP.many1 RP.get
    let dist = read distChar
    return $ Motion dir dist
  readListPrec = readListPrecDefault

motionsToUnitaryDirections :: [Motion] -> [Direction]
motionsToUnitaryDirections [] = []
motionsToUnitaryDirections (Motion m n : ls) =
  replicate n m ++ motionsToUnitaryDirections ls

---- Coordinates

type Position = Int

type Coor = (Position, Position)

---- Da local friendly state machine

type PairCoor = (Coor, Coor)

data RBState = RBState PairCoor PairCoor

runMotions :: [Motion] -> ([Coor], [Coor])
runMotions ms =
  let directions = motionsToUnitaryDirections ms
      (positions, _) = runState (traverse run directions) ((0, 0), (0, 0))
   in unzip positions

run :: Direction -> State PairCoor PairCoor
run dir = state $ \(h, t) ->
  let h' = moveHead h dir
      next =
        if tooFar h' t
          then (h', h)
          else (h', t)
   in (next, next)

moveHead :: Coor -> Direction -> Coor
moveHead (x, y) U = (x + 1, y)
moveHead (x, y) D = (x - 1, y)
moveHead (x, y) R = (x, y + 1)
moveHead (x, y) L = (x, y - 1)

tooFar :: Coor -> Coor -> Bool
tooFar (hX, hY) (tX, tY) = max (abs (tX - hX)) (abs (tY - hY)) > 1

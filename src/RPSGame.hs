module RPSGame (RPS (..), RPSResult (..), play, inversePlay) where

import Data.Functor (($>))
import Text.ParserCombinators.ReadPrec
  ( ReadPrec,
    choice,
    get,
    pfail,
  )
import Text.Read
  ( Read (readListPrec, readPrec),
    readListPrecDefault,
  )

data RPSResult = Win | Draw | Lose
  deriving (Show, Eq)

data RPS = Rock | Paper | Scissors
  deriving (Show, Eq)

parse :: Traversable t => t Char -> b -> ReadPrec b
parse str typ = traverse pChar str $> typ
  where
    pChar c = do
      c' <- get
      if c == c'
        then return c
        else pfail

instance Read RPS where
  -- TO
  readPrec =
    choice
      [ parse "Rock" Rock,
        parse "A" Rock,
        parse "X" Rock,
        parse "Paper" Paper,
        parse "B" Paper,
        parse "Y" Paper,
        parse "Scissors" Scissors,
        parse "C" Scissors,
        parse "Z" Scissors
      ]
  readListPrec = readListPrecDefault

instance Read RPSResult where
  readPrec =
    choice
      [ parse "Win" Win,
        parse "Z" Win,
        parse "Draw" Draw,
        parse "Y" Draw,
        parse "Lose" Lose,
        parse "X" Lose
      ]
  readListPrec = readListPrecDefault

play :: RPS -> RPS -> RPSResult
play Rock Rock = Draw
play Rock Paper = Lose
play Rock Scissors = Win
play Paper Paper = Draw
play Paper Scissors = Lose
play Scissors Scissors = Draw
play a b = opposite $ play b a
  where
    opposite Win = Lose
    opposite Lose = Win
    opposite Draw = Draw

inversePlay :: RPS -> RPSResult -> RPS
inversePlay rps Draw = rps
inversePlay Rock Win = Paper
inversePlay Rock Lose = Scissors
inversePlay Paper Win = Scissors
inversePlay Paper Lose = Rock
inversePlay Scissors Win = Rock
inversePlay Scissors Lose = Paper

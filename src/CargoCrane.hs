{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ViewPatterns #-}

module CargoCrane
  ( buildCargo,
    readCraneInstructions,
    followInstruction,
    instructionSet9000,
    instructionSet9001,
  )
where

import Control.Lens (element, set)
import Data.Attoparsec.ByteString.Char8
  ( char,
    digit,
    many',
    many1,
    parseOnly,
    string,
  )
import qualified Data.ByteString.Char8 as BS
import Data.List (transpose, uncons)
import Data.Maybe (fromJust)

type Crate = Char

type Stack = [Crate]

type Cargo = [Stack]

type InstructionSet = Int -> Int -> Int -> [Instruction]

data Instruction = Instruction {iFrom :: Int, iTo :: Int, ihowMany :: Int}
  deriving (Eq, Show)

-- Builds a list of stacks from an International Standard Elf cargo map
buildCargo :: [String] -> [Stack]
buildCargo =
  reverse . map clean . transpose . map (getMapRow [] . (' ' :))
  where
    getMapRow acc [] = acc
    -- every 3th char out of 4 is the crate code
    getMapRow acc (_ : _ : c : _ : r) = getMapRow (c : acc) r
    getMapRow _ _ = error "Problem in map row"
    clean (' ' : r) = clean r
    clean r = r

instructionSet9000 :: InstructionSet
instructionSet9000 howManyMoves moveFrom moveTo =
  replicate howManyMoves $ Instruction moveFrom moveTo 1

instructionSet9001 :: InstructionSet
instructionSet9001 howManyMoves moveFrom moveTo =
  [Instruction moveFrom moveTo howManyMoves]

{-# ANN readCraneInstruction ("HLint: ignore Use <$>" :: String) #-}
readCraneInstruction :: InstructionSet -> BS.ByteString -> Either String [Instruction]
readCraneInstruction is = parseOnly $ do
  _ <- parseWord "move"
  howMany <- parseNumber
  _ <- parseWord "from"
  readFrom <- parseNumber
  _ <- parseWord "to"
  readTo <- parseNumber
  return $ is howMany readFrom readTo
  where
    parseNumber = read <$> many1 digit
    parseWord w = many' (char ' ') >> string w >> many1 (char ' ')

readCraneInstructions :: InstructionSet -> [String] -> [Instruction]
readCraneInstructions is =
  concat
    . either (error . ("Failure in reading instructions: " ++)) id
    . traverse (readCraneInstruction is . BS.pack)

followInstruction :: Cargo -> Instruction -> Cargo
followInstruction cargo (Instruction (subtract 1 -> f) (subtract 1 -> t) 1) =
  let (e, restFrom) = fromJust . uncons $ cargo !! f
      originalTo = cargo !! t
   in set (element f) restFrom $ set (element t) (e : originalTo) cargo
followInstruction cargo (Instruction (subtract 1 -> f) (subtract 1 -> t) n) =
  let (startFrom, restFrom) = splitAt n $ cargo !! f
      originalTo = cargo !! t
   in set (element f) restFrom $ set (element t) (startFrom ++ originalTo) cargo

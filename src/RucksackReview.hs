module RucksackReview (getCommon, getPriority, getCommonBadge) where

import Data.Char (isLower, ord)
import Data.List (intersect)

getCommon :: String -> Char
getCommon str =
  let len = length str `div` 2
      (f, s) = splitAt len str
   in head $ intersect f s

getPriority :: Char -> Int
getPriority c
  | isLower c = ord c - 96
  | otherwise = ord c - 38

getCommonBadge :: [String] -> Char
getCommonBadge (a : b : c : _) = head $ a `intersect` b `intersect` c
getCommonBadge _ = error "Should have groups of three"

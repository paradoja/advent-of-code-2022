module CommunicationSystem (findStartOfPacketMarker, findStartOfMessageMarker) where

import Data.List (nub)

findSignalMarkerH :: Int -> Int -> String -> Maybe Int
findSignalMarkerH howManyToTake acc signal@(_ : r) =
  let list = take howManyToTake signal
   in if nub list == list -- && length list == howManyToTake
        then Just $ acc + howManyToTake - 1
        else findSignalMarkerH howManyToTake (acc + 1) r
findSignalMarkerH _ _ _ = Nothing

findStartOfPacketMarker :: String -> Maybe Int
findStartOfPacketMarker = findSignalMarkerH 4 1

findStartOfMessageMarker :: String -> Maybe Int
findStartOfMessageMarker = findSignalMarkerH 14 1

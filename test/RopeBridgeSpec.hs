module RopeBridgeSpec where

import Data.List (nub)
import RopeBridge
import Test.Hspec

directions1 :: [Motion]
directions1 =
  fmap
    read
    [ "R 4",
      "U 4",
      "L 3",
      "D 1",
      "R 4",
      "D 1",
      "L 5",
      "R 2"
    ]

positions1 :: ([Coor], [Coor])
positions1 = runMotions directions1

spec :: Spec
spec = do
  describe "runMotions with directions1" $ do
    it "give the correct amount of positions passed" $ do
      length (nub $ snd positions1) `shouldBe` 13

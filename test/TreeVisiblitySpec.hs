module TreeVisiblitySpec where

import Data.List (transpose)
import Test.Hspec
import TreeVisibility

input1, input1t :: [String]
input1 =
  [ "30373",
    "25512",
    "65332",
    "33549", -- 1 1 1 1
    "35390" -- 3 1 1 1
  ]
input1t = transpose input1

input2, input2t :: [String]
input2 =
  [ "303713",
    "255112",
    "653312",
    "335419", -- -1 -2
    "353910", -- 4 1 3 2
    "353910"
  ]
input2t = transpose input2

spec :: Spec
spec = do
  describe "scenicScoreFor with input2" $ do
    it "halfScenicScore 4 3" $ do
      scenicScoreFor input2 input2t 4 3 `shouldBe` 24
  describe "scenicScoreFor with input1" $ do
    it "works for test 1 2" $ do
      scenicScoreFor input1 input1t 1 2 `shouldBe` 4
    it "works for test 3 2" $ do
      scenicScoreFor input1 input1t 3 2 `shouldBe` 8
    it "works for test 3 3" $ do
      scenicScoreFor input1 input1t 3 3 `shouldBe` 3
    it "works for test 3 1" $ do
      scenicScoreFor input1 input1t 3 1 `shouldBe` 1
  describe "maximizingTrees" $ do
    it "works for input1" $
      maximizingTrees input1 `shouldBe` 8

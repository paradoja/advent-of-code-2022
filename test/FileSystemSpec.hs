{-# LANGUAGE OverloadedStrings #-}

module FileSystemSpec where

import FileSystem
import Test.Hspec

testEFS :: ElfFileSystem
testEFS =
  Directory
    "/"
    [ Directory
        "a"
        [ Directory "b" [File "z" 100],
          File "y" 200
        ],
      File "x" 350
    ]

spec :: Spec
spec = do
  describe "totalSize" $ do
    it "works for files" $
      totalSize (File "name" 123) `shouldBe` 123
    it "works for directories" $
      totalSize (Directory "dir" [File "name" 123]) `shouldBe` 123
    it "works with trees of directories" $
      totalSize testEFS `shouldBe` 650

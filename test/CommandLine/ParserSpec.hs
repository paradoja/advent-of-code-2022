{-# LANGUAGE OverloadedStrings #-}

module CommandLine.ParserSpec where

import CommandLine.Parser
  ( CdTarget (Up),
    Command (CD, LS),
    ElfListing,
    ListingItem (Command, FS),
    parseListing,
  )
import Data.Attoparsec.ByteString.Char8 (Parser, feed, maybeResult, parse)
import qualified Data.ByteString.Char8 as BS
import Data.Maybe (fromJust)
import FileSystem (ElfFileSystem, FileSystem (Directory, File))
import Test.Hspec (Spec, describe, it, shouldBe)

lsResult :: BS.ByteString
lsResult =
  BS.unlines
    [ "dir e",
      "29116 f",
      "2557 g",
      "62596 h.lst"
    ]

lsListing :: BS.ByteString
lsListing =
  BS.unlines
    [ "$ ls",
      lsResult,
      "$ cd ..",
      "$ ls",
      lsResult
    ]

dirList :: [ElfFileSystem]
dirList = [Directory "e" [], File "f" 29116, File "g" 2557, File "h.lst" 62596]

listing :: ElfListing
listing = [Command LS] ++ fmap FS dirList ++ [Command $ CD Up, Command LS] ++ fmap FS dirList

-- quickparse - partial and insecure
qp :: Parser a -> BS.ByteString -> a
qp p = fromJust . maybeResult . flip feed mempty . parse p

spec :: Spec
spec = do
  describe "Parsing Listings" $ do
    it "works for simple listing" $ do
      qp parseListing "$ ls" `shouldBe` [Command LS]
    it "works for a somewhat complex listing" $
      qp parseListing lsListing `shouldBe` listing
